const _ = require('lodash');
const mongoHelperFunctions = require('../utils/mongoHelperFunctions');

const acceptedFields = ['title', 'description', 'status'],
    acceptedStatus = ['pending', 'started', 'completed'];

async function createItem(req, res) {
    let validationResult = validateCreateFunc(req);
    if (!validationResult.valid)
        return res.badRequest(validationResult.errorMessasge)

    let { body } = req;
    console.log('Got new request for creating item for to do list');
    try {
        await ToDoListService.insetItem(body);
        console.log('Created new item');
        return res.send(204);
    } catch (error) {
        console.log(`Internal server error: ${error}`);
        return res.serverError(error);
    }
}

async function deleteItem(req, res) {
    let { id } = req.params;
    console.log(`Got new request for deleting ${id} item from to do list`);

    if (!mongoHelperFunctions.validateMongoId(id))
        return res.badRequest('invalid mongo id in the request');
    try {
        await ToDoListService.deleteItem(id); // do something with item??
        console.log(`_id: ${id} deleted successfully`);

        return res.send(204); // No Content Status Code
    } catch (error) {
        console.log(`Internal server error: ${error}`);
        return res.serverError(error);
    }
}

async function getItem(req, res) { // validate input?
    let { id } = req.params;
    console.log(`Got new request for get ${id} item from to do list`);

    if (!mongoHelperFunctions.validateMongoId(id))
        return res.badRequest('invalid mongo id in the request');
    try {
        let item = await ToDoListService.getItemById(id); // do something with item??
        if (_.isNil(item))
            return res.notFound();
        return res.ok(item);
    } catch (error) {
        console.log(`Internal server error: ${error}`);
        return res.serverError(error);
    }
}

async function updateItem(req, res) {
    let { id } = req.params;
    console.log(`Got new request for update ${id} item from to do list`);

    let validationResult = validateUpdateFunc(req, id);
    if (!validationResult.valid)
        return res.badRequest(validationResult.errorMessasge)
    try {
        await ToDoListService.updateItem(id, req.body);
        return res.send(204);
    } catch (error) {
        console.log(`Internal server error: ${error}`);
        return res.serverError(error);
    }
}

async function getAllItems(req, res) {
    console.log(`Got new request for get all items from to do list`);
    try {
        let items = await ToDoListService.getAllItems();
        if (_.isEmpty(items))
            return res.ok('There is not to do items in the list');
        return res.ok(items);
    } catch (error) {
        console.log(`Internal server error: ${error}`);
        return res.serverError(error);
    }
}

async function getItemsByStatus(req, res) {
    let validationResult = validateGetByStatusFunc(req);

    if (!validationResult.valid)
        return res.badRequest(validationResult.errorMessasge);

    let { status } = req.query;
    console.log(`Got new request for get item by status ${status} from to do list`);
    try {
        let items = await ToDoListService.getItemsByStatus(status);
        return res.ok(items);
    } catch (error) {
        console.log(`Internal server error: ${error}`);
        return res.serverError(error);
    }
}

// ** Validation Functions ** //

function validateUpdateFunc(req, id) {
    let { body } = req,
        validKeys = true,
        status;

    Object.keys(body).forEach(key => {
        if (!_.includes(acceptedFields, key)) {
            validKeys = false
        }
        if (key === 'status') status = body[key];
    })
    if (!mongoHelperFunctions.validateMongoId(id)) {
        return {
            errorMessasge: 'invalid mongo id in the request',
            valid: false
        }
    }
    if (!validKeys) {
        return {
            errorMessasge: `${acceptedFields} only can be updated`,
            valid: validKeys
        }
    }
    if (!_.isNil(status) && !_.includes(acceptedStatus, status)) {
        return {
            errorMessasge: `cannot update to other status then ${acceptedStatus}`,
            valid: false
        }
    }
    return { valid: true };
}

function validateGetByStatusFunc(req) {
    if (!('status' in req.query)) {
        return {
            errorMessasge: 'This route must contain status query',
            valid: false
        }
    }
    if (!_.includes(acceptedStatus, req.query.status)) {
        return {
            errorMessasge: `valid statuses are : ${acceptedStatus}`,
            valid: false
        }
    }
    return { valid: true };
}

function validateCreateFunc(req) {
    if (_.difference(acceptedFields, Object.keys(req.body)).length != 0) {
        return {
            errorMessasge: `body must contain the keys: ${acceptedFields}`,
            valid: false
        }
    }
    return { valid: true };
}

module.exports = {
    getItemsByStatus,
    getAllItems,
    updateItem,
    getItem,
    deleteItem,
    createItem
};