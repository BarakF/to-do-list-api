const { connect, Types } = require('mongoose'),
    _ = require('lodash');

module.exports = {
    connectMongo: async (host, database, username, password) => {
        if (_.isNil(host) || _.isNil(database)) {
            console.log('Mongo paramters are not valid');
            return Promise.resolve();
        }
        try {
            let uri = `mongodb://${username}:${password}@${host}:27017/${database}`;

            await connect(uri, { useNewUrlParser: true });
            console.log(`Connected to mongo, host: ${host}, database: ${database}`)
        } catch (error) {
            throw error;
        }
    },

    validateMongoId: (id) => {
        return Types.ObjectId.isValid(id);
    }
}