const mongoose = require('mongoose');
const { Schema } = mongoose;

const toDoListSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
}, { collection: 'to_do_list', timestamps: true, versionKey: false });

let model = mongoose.model('ToDoListSchema', toDoListSchema);

module.exports = model;