const cache = require('node-cache'),
    _ = require('lodash'),
    ToDoList = require('../utils//schemas/toToListSchema');

const TO_DO_LOST_PROJECTION = 'title description status createdAt',
    CACHE_TTL = 300;//5m
const CACHE = new cache();

module.exports.insetItem = async function (value) {
    let item = await ToDoList.create(value),
        allItemsCached;
    if (item && !CACHE.has(item._id.toString())) {
        delete item._doc.updatedAt;
        CACHE.set(item._id.toString(), item, CACHE_TTL)
    }
    allItemsCached = CACHE.get('all')
    if (!_.isNil(allItemsCached)) {
        allItemsCached.push(item);
        CACHE.set('all', allItemsCached, CACHE_TTL);
    }
    return item;
}

module.exports.deleteItem = async function (id) {
    let allItemsCached,
        itemToDelete;
    await ToDoList.findByIdAndDelete(id);
    if (CACHE.has(id))
        CACHE.del(id);
    allItemsCached = CACHE.get('all')
    itemToDelete = _.filter(allItemsCached, item => {
        return item._id.toString() == id;
    });
    if (!_.isNil(allItemsCached) && !_.isNil(itemToDelete)) {
        allItemsCached = _.filter(allItemsCached, item => { return item._id != id });
        CACHE.set('all', allItemsCached, CACHE_TTL);
    }
    return;
}

module.exports.getItemById = async function (id) {
    let item = CACHE.get(id);
    if (!_.isNil(item)) {
        CACHE.ttl(item._id.toString(), CACHE_TTL); // refresh TTL
        return item;
    }

    item = await ToDoList.findOne({ _id: id })
        .select(TO_DO_LOST_PROJECTION);
    if (!_.isNil(item))
        CACHE.set(item._id.toString(), item, CACHE_TTL);
    return item;
}

module.exports.getAllItems = async function () {
    let allItems;

    if (CACHE.has('all')) {
        // CACHE.ttl('all', CACHE_TTL)
        return CACHE.get('all');
    }
    allItems = await ToDoList.find({})
        .select(TO_DO_LOST_PROJECTION);
    CACHE.set('all', allItems, CACHE_TTL)
    return allItems;
}

module.exports.getItemsByStatus = function (status) {
    let allItems = CACHE.get('all');
    if (!_.isNil(allItems)) {
        let itemsByStatus = _.map(allItems, item => {
            return item.status === status ? item : null
        });
        return _.compact(itemsByStatus);
    }
    return ToDoList.find({ status: status })
        .select(TO_DO_LOST_PROJECTION);
}

module.exports.updateItem = async function (id, body) {
    let allItemsCached,
        itemToDelete;
    await ToDoList.updateOne({ _id: id }, body)
    if (CACHE.has(id))
        CACHE.del(id);
    allItemsCached = CACHE.get('all')
    itemToDelete = _.filter(allItemsCached, item => {
        return item._id.toString() == id;
    });
    if (!_.isNil(allItemsCached) && !_.isNil(itemToDelete)) {
        allItemsCached = _.filter(allItemsCached, item => { return item._id != id });
        CACHE.set('all', allItemsCached, CACHE_TTL);
    }
    return;
}