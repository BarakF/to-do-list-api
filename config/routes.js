/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  'GET /list/item/:id': 'ToDoListController.getItem',
  'GET /list/item/all': 'ToDoListController.getAllItems',
  'GET /list/item': 'ToDoListController.getItemsByStatus',
  'POST /list/item': 'ToDoListController.createItem',
  'DELETE /list/item/:id': 'ToDoListController.deleteItem',
  'PUT /list/item/:id': 'ToDoListController.updateItem',
};
