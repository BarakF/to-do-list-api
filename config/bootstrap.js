require('dotenv').config(),
  sails = require('sails');

const { connectMongo } = require('../api/utils/mongoHelperFunctions');
/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function (cb) {
  const MONGO_HOST = sails.config.setting.mongo.host,
    MONGO_USERNAME = sails.config.setting.mongo.username,
    MONGO_PASSWORD = sails.config.setting.mongo.password,
    MONGO_DATABASE = sails.config.setting.mongo.database

  try {
    cb();
    await connectMongo(MONGO_HOST, MONGO_DATABASE, MONGO_USERNAME, MONGO_PASSWORD);
  } catch (error) {
    console.log(`Error occured while tryng load bootstap: ${error}`);
    process.exit(1);
  }
};
