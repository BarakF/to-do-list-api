require('dotenv').config();
var sails = require('sails'),
    _ = require('lodash');

const { connectMongo } = require('../api/utils/mongoHelperFunctions'),
    ToDoListSchema = require('../api/utils/schemas/toToListSchema');

const MONGO_HOST = process.env.MONGO_HOST,
    MONGO_DATABASE = process.env.MONGO_DATABASE,
    MONGO_USERNAME = process.env.MONGO_USERNAME,
    MONGO_PASSWORD = process.env.MONGO_PASSWORD

before(function (done) {
    this.timeout(10000);

    sails.lift({
        environment: 'testing',
        hooks: {
            grunt: false,
            socket: false,
            pubsub: false,
            session: false
        }
    }, async function (err, sails) {
        if (err) {
            return done(err);
        }
        await connectMongo(MONGO_HOST, sails.config.setting.mongo.database, MONGO_USERNAME, MONGO_PASSWORD, { useUnifiedTopology: true });
        await wipeMock();
        done(err, sails);
    });
});

after(function () {
    wipeMock()
        .then(() => sails.lower())
});

function wipeMock() {
    return ToDoListSchema.deleteMany({});
}