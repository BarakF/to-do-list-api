module.exports = [
    {
        title: "example1",
        description: "example1 desc",
        status: "started"
    },
    {
        title: "example2",
        description: "example2 desc",
        status: "started"
    },
    {
        title: "example3",
        description: "example3 desc",
        status: "pending"
    },
    {
        title: "example4",
        description: "example4 desc",
        status: "pending"
    },
    {
        title: "example5",
        description: "example5 desc",
        status: "completed"
    },
    {
        title: "example6",
        description: "example6 desc",
        status: "completed"
    }
]