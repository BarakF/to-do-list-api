let chai = require('chai'),
    chaiHttp = require('chai-http'),
    itemsMock = require('./itemsMock'),
    ToDoListSchema = require('../api/utils/schemas/toToListSchema'),
    expect = chai.expect;

chai = chai.use(chaiHttp);
const BASE_API_ROUTE = '/list/item',
    INVALID_MONOGO_ID = '6050f6a60c1c534a04d47',
    MONGO_ID_NOT_EXISTS = '6052274602ad774378dde267',
    INVALID_ITEM = {
        title: "Barak",
        desciption: "Barak desc",
        wrongField: "pending"
    },
    ITEM_WITH_MISSING_FILE = {
        title: "Barak",
        status: "completed"
    },
    ITEM_WITH_INVALID_STATUS = {
        title: "Barak",
        desciption: "Barak desc",
        status: "badStatus"
    },
    VALID_ITEM = {
        title: "BarakItem",
        description: "Barak desc",
        status: "started"
    };

describe('#ToDoList Controller tests', function () {
    describe('#Insertion tests', function () {
        this.timeout(10000);

        beforeEach(async function () {
            await insertToMongo();
        });
        afterEach(async function () {
            await wipeMongo()
        });
        it('should return bad request because missing fields in body', function (done) {
            chai.request(sails.hooks.http.app)
                .post(`${BASE_API_ROUTE}`)
                .send(ITEM_WITH_MISSING_FILE)
                .then(res => {
                    expect(res).to.have.status(400);
                    done();
                })
                .catch(error => {
                    done(error);
                })
        })
        it('should return bad request because of wrong field in body', function (done) {
            chai.request(sails.hooks.http.app)
                .post(`${BASE_API_ROUTE}`)
                .send(INVALID_ITEM)
                .then(res => {
                    expect(res).to.have.status(400);
                    done();
                })
                .catch(error => {
                    done(error);
                })
        })
        it('should return bad request because of wrong status', function (done) {
            chai.request(sails.hooks.http.app)
                .post(`${BASE_API_ROUTE}`)
                .send(ITEM_WITH_INVALID_STATUS)
                .then(res => {
                    expect(res).to.have.status(400);
                    done();
                })
                .catch(error => {
                    done(error);
                })
        })
        it('should return 204 and save the new item', function (done) {
            chai.request(sails.hooks.http.app)
                .post(`${BASE_API_ROUTE}`)
                .send(VALID_ITEM)
                .then(res => {
                    let itemInserted;
                    ToDoListSchema.findOne({ title: "BarakItem" })
                        .then((item) => {
                            itemInserted = item ? true : false;
                            expect(itemInserted).to.be.equal(true);
                            expect(res).to.have.status(204);
                            done();
                        })
                })
                .catch(error => {
                    done(error);
                })
        })
    });
    describe('#Deletion tests', function () {
        this.timeout(10000);
        beforeEach(async function () {
            await insertToMongo();
        });
        afterEach(async function () {
            await wipeMongo()
        });
        it('should return 400 when delete with invalid mongo id', function (done) {
            chai.request(sails.hooks.http.app)
                .delete(`${BASE_API_ROUTE}/${INVALID_MONOGO_ID}`)
                .then(res => {
                    expect(res).to.have.status(400);
                    done();
                })
                .catch(error => {
                    done(error);
                })
        })
        it('should return 204 and delete item', async function () {
            let item = await ToDoListSchema.findOne({ title: 'example1' })
            let mongoIdToDelete = item._id.toString();

            chai.request(sails.hooks.http.app)
                .delete(`${BASE_API_ROUTE}/${mongoIdToDelete}`)
                .then(res => {
                    ToDoListSchema.findById(mongoIdToDelete)
                        .then((deletedItem) => {
                            expect(deletedItem).to.equal(null);
                            expect(res).to.have.status(204);
                            return Promise.resolve();
                        })
                })
                .catch(error => {
                    done(error);
                })
        })
    })
    describe(`#GET tests`, () => {
        beforeEach(async function () {
            await insertToMongo();
        });
        afterEach(async function () {
            await wipeMongo()
        });
        it('should return 400 when get with invalid mongo id', function (done) {
            chai.request(sails.hooks.http.app)
                .get(`${BASE_API_ROUTE}/${INVALID_MONOGO_ID}`)
                .then(res => {
                    expect(res).to.have.status(400);
                    done();
                })
                .catch(error => {
                    done(error);
                })
        })
        it('should return 404 when get with mongo id which doesnt exists', function (done) {
            chai.request(sails.hooks.http.app)
                .get(`${BASE_API_ROUTE}/${MONGO_ID_NOT_EXISTS}`)
                .then(res => {
                    expect(res).to.have.status(404);
                    done();
                })
                .catch(error => {
                    done(error);
                })
        })
        it('should return 200 get an item', function (done) {
            ToDoListSchema.findOne({ title: 'example1' })
                .then((item) => {
                    chai.request(sails.hooks.http.app)
                        .get(`${BASE_API_ROUTE}/${item._id.toString()}`)
                        .then(res => {
                            expect(res.body._id).to.be.equal(item._id.toString());
                            expect(res).to.have.status(200);
                            done();
                        })
                        .catch(error => {
                            done(error);
                        })
                })
        })
        it('should return 404 when get with mongo id which doesnt exists', function (done) {
            chai.request(sails.hooks.http.app)
                .get(`${BASE_API_ROUTE}/${MONGO_ID_NOT_EXISTS}`)
                .then(res => {
                    expect(res).to.have.status(404);
                    done();
                })
                .catch(error => {
                    done(error);
                })
        })
        it('should return 200 with all items', function (done) {
            chai.request(sails.hooks.http.app)
                .get(`${BASE_API_ROUTE}/all`)
                .then(res => {
                    expect(res.body.length).to.equal(itemsMock.length)
                    expect(res).to.have.status(200);
                    done();
                })
                .catch(error => {
                    done(error);
                })
        })
        it('should return 400 when request with bad status param', function (done) {
            chai.request(sails.hooks.http.app)
                .get(`${BASE_API_ROUTE}/?status=badSTATUS`)
                .then(res => {
                    expect(res).to.have.status(400);
                    done();
                })
                .catch(error => {
                    done(error);
                })
        })
        it('should return 200 with two missions with status completed', function (done) {
            chai.request(sails.hooks.http.app)
                .get(`${BASE_API_ROUTE}/?status=completed`)
                .then(res => {
                    expect(res.body.length).to.equal(2);
                    expect(res.body[0].status).to.equal('completed');
                    expect(res.body[1].status).to.equal('completed');
                    expect(res).to.have.status(200);
                    done();
                })
                .catch(error => {
                    done(error);
                })
        })

    })
    describe('#Update tests', () => {
        this.timeout(10000);
        beforeEach(async function () {
            await insertToMongo();
        });
        afterEach(async function () {
            await wipeMongo()
        });
        it('should return 400 when update with invalid mongo id', function (done) {
            chai.request(sails.hooks.http.app)
                .put(`${BASE_API_ROUTE}/${INVALID_MONOGO_ID}`)
                .send(VALID_ITEM)
                .then(res => {
                    expect(res).to.have.status(400);
                    done();
                })
                .catch(error => {
                    done(error);
                })
        })
        it('should return 400 when update with invalid field', function (done) {
            ToDoListSchema.findOne({ title: 'example1' })
                .then((item) => {
                    let inValidUpdateBody = {
                        title: 'Good Title',
                        desciption: 'Good Desc',
                        invalidField: 'Bad Field'
                    }
                    chai.request(sails.hooks.http.app)
                        .put(`${BASE_API_ROUTE}/${item._id.toString()}`)
                        .send(inValidUpdateBody)
                        .then(res => {
                            expect(res).to.have.status(400);
                            done();
                        })
                        .catch(error => {
                            done(error);
                        })
                })
        })
        it('should return 400 when update with invalid status', function (done) {
            ToDoListSchema.findOne({ title: 'example1' })
                .then((item) => {
                    let inValidUpdateBody = {
                        title: 'Good Title',
                        desciption: 'Good Desc',
                        status: 'Status different from started/pending/completed'
                    }
                    chai.request(sails.hooks.http.app)
                        .put(`${BASE_API_ROUTE}/${item._id.toString()}`)
                        .send(inValidUpdateBody)
                        .then(res => {
                            expect(res).to.have.status(400);
                            done();
                        })
                        .catch(error => {
                            done(error);
                        })
                })
        })
        it('should return 204 and update item status to completed', function (done) {
            ToDoListSchema.findOne({ title: 'example1' })
                .then((item) => {
                    let updatedItem = {
                        status: 'completed'
                    },
                        mongoId = item._id.toString();
                    chai.request(sails.hooks.http.app)
                        .put(`${BASE_API_ROUTE}/${item._id.toString()}`)
                        .send(updatedItem)
                        .then(res => {
                            ToDoListSchema.findById(mongoId)
                                .then((updatedItem) => {
                                    expect(updatedItem.status).to.equal('completed')
                                    expect(res).to.have.status(204);
                                    done();
                                })
                        })
                        .catch(error => {
                            done(error);
                        })
                })
        })
    })
});

async function insertToMongo() {
    return ToDoListSchema.insertMany(itemsMock);
}

function wipeMongo() {
    return ToDoListSchema.deleteMany({});
}