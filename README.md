# To Do list API

This API used to manage To-Do list items, it supports all CRUD operations, filtering items by status, and get all items upon request.
All items created by this API will be saved in MongoDB database, and cache is also supported to improve the performance of GET operations.
The API implemented using Sails.js platform, for more information you can use this link:	https://sailsjs.com/documentation/reference 

## Installation

```bash
npm install

## Usage

Run Npm start - to lift the app
Run Npm test – run unit tests

```

## Project structure
/api/controllers –contains ToDoList Controller.\
/api/services – contains ToDoList Service.\
/api/utils – contains MongoDB files.\
/config – contains all configuration files, an important files included in this folders are:\
	/env – includes env configuration for testing and development environments.\
	&nbsp;&nbsp;&nbsp;&nbsp; Bootstrap.js – this file will execute first and includes mongodb connect operation.\
	&nbsp;&nbsp;&nbsp;&nbsp; /routes – this files contains all the supported routes for this Api.\
&nbsp;&nbsp;&nbsp;&nbsp; /test – contains test files.\
&nbsp;&nbsp;&nbsp;&nbsp; .env – environment variables that will be injected to the app.\

## Routes
'GET /list/item/:id' => Get a single item by mongo id\
  'GET /list/item/all' => Get all items\
  'GET /list/item' => Get item by status: example: /list/item?status=pending\
  'POST /list/item' => Create new item\
  'DELETE /list/item/:id' => Delete item\
  'PUT /list/item/:id' => Update item


## Scalability
![Click here!](https://ibb.co/Pj6Q49T) \
Explanation:\
API requests will be sent to the API services from NGINX servers. Those requests is also sent to RabbitMQ queue and if the auto-scaler recognized that the queue is higher than defined threshold, that auto-scaler create more containers of the API services. 
